# README #

### VUE-POLAR / Statemanager ###
#####V1.0.2
#####Typescript required

The "Vue-polar / middleware" is an extension of "vue-router". This package adds the addition to implement middleware 
classes in a neat typescript way.

### How do I get set up? ###

#####Usage with yarn
<code>yarn add @vue-polar/middleware</code>

#####Usage with npm
<code>npm i @vue-polar/middleware</code>

#####Dependencies
* "vue": "^2.6.11",
* "vue-router": "^3.1.5"
#####Dev Dependencies
* "typescript": "~3.7.5",
#####ESlint Configuration
* "@typescript-eslint/no-explicit-any": "off",
#####Usage
When you first setup "@vue-polar/middleware" you need to add the following lines to the bottom of your "@/routes/index.ts"
just above the export
```typescript
//Inject the router
MiddlewareRouter.setRouter(router);

//Here you will be able to register the middleware.
//You'll instantiate your middleware class and define how important it is
//The importance goes from 0 as not very important to higher as very important
//
//So this middleware is not that important
//MiddlewareRouter.register(new ExampleAfterMiddleware(),0);
//
//But this one is very important
//MiddlewareRouter.register(new ExampleBeforeMiddleware(),10);

//Call router functions
MiddlewareRouter.enable();
```

There are 2 types of Middleware:  After and Before.
In this package we define the difference by adding a Generic Type to the interface

Here is how that looks like:
```typescript
//Extends the MiddlewareContract with the AfterMiddleware GenericType
export class ExampleAfterMiddleware implements MiddlewareContract<AfterMiddleware>{

    //With middleware groups we can filter for which routes we need to run this middleware
    getMiddlewareGroups(): Array<string> {
        return ['someGroup'];
    }

    //because we are using the generic type after middleware
    //the class now expects you to create a call property with the exact implementation the vue router needs
    //in this case being:
    call: AfterMiddleware = (to, from) => {
        //asd
    }
}

//Below you will find an example for the Before version of a middleware

//Extends the MiddlewareContract with the BeforeMiddleware GenericType
export class ExampleBeforeMiddleware implements MiddlewareContract<BeforeMiddleware>{
    
    //With middleware groups we can filter for which routes we need to run this middleware
    getMiddlewareGroups(): Array<string> {
        return ['someGroup'];
    }


    //because we are using the generic type before middleware
    //the class now expects you to create a call property with the exact implementation the vue router needs
    //in this case being:
    call: BeforeMiddleware = (to, from, next) => {
        return true;
    }
}

```

Now last but not least you need to know how to create a route that responds to these middlewares:

```typescript
//we're using the default route system from vue-router
//the only difference is that we add a meta property
//This property is called 'middlewareGroups' and expects an array of middlewareGroup names
//Yes, These are the same as the ones you define in the 'getMiddlewareGroups()' method
{
    path: "/account",
    name: "account",
    component: Account,
    meta:{
      middlewareGroups: [
          'auth'
      ]
    }
}
```

### Contribution guidelines ###

We are currently still working on this package any suggestions and/or contribution is welcome

##### Writing tests
Writing tests should be done in JEST and written according TDD

##### Code review
We will review the code submitted before there is any permanent action taken.
Please do not waste my time on anything that is outside of the scope of this package.

### Who do I talk to? ###
stanley-leroy-gerrits
