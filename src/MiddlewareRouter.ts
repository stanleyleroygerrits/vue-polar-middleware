import { Route, RouteRecord, VueRouter } from "vue-router/types/router";
import MiddlewareContract from "./MiddlewareContract";
import {AfterMiddleware, BeforeMiddleware, MiddlewareImportance, MiddlewareImportanceList} from "./Types";

class MiddlewareRouter {

    afterMiddleware: MiddlewareImportanceList<AfterMiddleware> = [];
    beforeMiddleware: MiddlewareImportanceList<BeforeMiddleware> = [];
    router!: VueRouter;

    public setRouter(router: VueRouter) {
        this.router = router;
    }

    public register(middleware: MiddlewareContract<AfterMiddleware|BeforeMiddleware>, importance: number) {
        if(middleware.call.length === 2){
            this.afterMiddleware.push({
                middleware: middleware as MiddlewareContract<AfterMiddleware>,
                importance: importance
            });

            this.afterMiddleware.sort((a, b) => b.importance - a.importance)
        }

        if(middleware.call.length === 3){
            this.beforeMiddleware.push({
                middleware: middleware as MiddlewareContract<BeforeMiddleware>,
                importance: importance
            });

            this.beforeMiddleware.sort((a, b) => b.importance - a.importance)
        }
    }

    enable(): void {
        this.router.beforeEach((to: Route, from: Route, next: any) => {
            this.before(to, from, next);
        });

        this.router.afterEach((to: Route, from: Route) => {
            this.after(to, from);
        });
    }

    before(to: Route, from: Route, next: any) {

        let redirected = false;

        const routeMiddlewareGroups = this.getRouteMiddlewareGroups(to);

        this.beforeMiddleware.forEach((middlewareImportance: MiddlewareImportance<BeforeMiddleware>) => {

            const middleware = middlewareImportance.middleware;

            if (this.intersectMiddlewareGroups<BeforeMiddleware>(middleware, routeMiddlewareGroups)) return;

            if (middleware.call(to, from, next)) {
                redirected = true;
            }
        });

        if (!redirected) {
            next();
        }
    }

    after(to: Route, from: Route) {
        const routeMiddlewareGroups = this.getRouteMiddlewareGroups(to);

        this.afterMiddleware.forEach((middlewareImportance: MiddlewareImportance<AfterMiddleware>) => {

            const middleware = middlewareImportance.middleware;

            if (this.intersectMiddlewareGroups<AfterMiddleware>(middleware, routeMiddlewareGroups)) return;

            middleware.call(to, from);
        });
    }

    intersectMiddlewareGroups<T>(middleware: MiddlewareContract<T>, routeMiddlewareGroups: Array<string>) {
        const middlewareGroups = middleware.getMiddlewareGroups();

        const matchingMiddlewareGroups = middlewareGroups.filter(element => routeMiddlewareGroups.includes(element));

        return matchingMiddlewareGroups.length <= 0;
    }

    getRouteMiddlewareGroups(route: Route) {
        let routeMiddlewareGroups: Array<string> = [];

        route.matched.forEach((matchedRoute: RouteRecord) => {
            if (matchedRoute.meta.middlewareGroups) {
                routeMiddlewareGroups = routeMiddlewareGroups.concat(matchedRoute.meta.middlewareGroups);
            }
        });

        return routeMiddlewareGroups;
    }
}

export default new MiddlewareRouter();

