interface MiddlewareContract<T>
{
    getMiddlewareGroups(): Array<string>;

    call: T;
}

export default MiddlewareContract;
