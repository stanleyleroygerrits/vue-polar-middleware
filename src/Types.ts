import {Route} from "vue-router";
import MiddlewareContract from "./MiddlewareContract";

export interface MiddlewareImportance<T>{
    middleware: MiddlewareContract<T>;
    importance: number;
}
export type MiddlewareImportanceList<T> = Array<MiddlewareImportance<T>>;

export type AfterMiddleware = (to: Route, from: Route) => void;
export type BeforeMiddleware = (to: Route, from: Route, next: any) => boolean;
